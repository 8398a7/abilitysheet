# frozen_string_literal: true

module Score::Iidxme
  extend ActiveSupport::Concern

  included do
    def self.iidxme_sync(user_id, elems)
      user = User.find_by(id: user_id)
      elems.each do |elem|
        title = gigadelic_innocentwalls(elem['data'])
        title = title_check(title)
        sheet = Sheet.find_by(title: title)
        next unless sheet

        state = reverse(elem['clear'])
        next if state == 7

        score = user.scores.find_by(sheet_id: sheet.id, version: Abilitysheet::Application.config.iidx_version)
        score ||= user.scores.create!(sheet_id: sheet.id, version: Abilitysheet::Application.config.iidx_version)
        iidxme_params = { 'sheet_id' => sheet.id, 'state' => score.state, 'score' => elem['score'], 'bp' => elem['miss'] }
        iidxme_params['state'] = state if state <= score.state
        score.update_with_logs(iidxme_params)
      end
      true
    end

    def self.title_check(title)
      return title if Sheet.exists?(title: title)

      case title
      when %(キャトられ♥恋はモ～モク) then title = %(キャトられ恋はモ～モク)
      when %(旋律のドグマ～Misérables～) then title = %(旋律のドグマ ～Misérables～)
      when %(表裏一体！？怪盗いいんちょの悩み♥) then title = %(表裏一体！？怪盗いいんちょの悩み)
      end
      title
    end

    def self.gigadelic_innocentwalls(elem)
      title = elem['title']
      return title if title != 'gigadelic' && title != 'Innocent Walls'

      title += elem['diff'] == 'sph' ? '[H]' : '[A]'
      title
    end

    def self.reverse(state)
      status = %w[7 6 5 4 3 2 1 0]
      status[state].to_i
    end
  end
end
